from services.spotify import Spotify
import services.redis as Redis
from math import ceil


OUTPUT_PATH = "/tmp/unknown_related_artists.txt"


if __name__ == "__main__":
    """
    Loop through all artists indexed in Redis.
    For each artist, determine which direct related artists are not indexed.
    Accumulate these and write to disk, which can later be read by indexer.
    """

    spotify_client = Spotify.get_basic_client()
    unknown_related_artist_ids = set()
    all_artist_ids = set(Redis.get_all_artist_ids())
    num_artists = len(all_artist_ids)
    progress_interval = max(int(num_artists * 0.05), 1)
    for i, artist_id in enumerate(all_artist_ids):
        if i % progress_interval == 0:
            print("Progress: {}%".format(ceil((i / num_artists) * 100)))

        artist = Redis.get_artist(artist_id=artist_id)
        related_artist_ids = artist["related_artists"]
        for related_artist_id in related_artist_ids:
            if related_artist_id not in all_artist_ids:
                # Make sure Spotify API returns an artist with a matching id
                related_artist = spotify_client.get_artist(artist_id=related_artist_id)
                if related_artist.get("id") == related_artist_id:
                    unknown_related_artist_ids.add(related_artist_id)

    if unknown_related_artist_ids:
        print("Found {} unknown related artists".format(len(unknown_related_artist_ids)))
        with open(OUTPUT_PATH, "w") as f:
            f.write(",".join(unknown_related_artist_ids))
        print("Unknown artists written to {}".format(OUTPUT_PATH))
