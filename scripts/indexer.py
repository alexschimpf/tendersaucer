import random
import string
import traceback
import time
from utils import ConcurrentSet
import services.redis as Redis
from services.spotify import Spotify
from genres import Genre
from concurrent.futures import ThreadPoolExecutor, as_completed


NUM_WORKERS = 6
DELAYED_INDEX_TIME_LIMIT = 60
WORKER_TIME_LIMIT = 0.5 * 60 * 60
LOAD_VISITED_WITH_REDIS_ARTISTS = False
PICK_SEEDS_FROM_UNKNOWN_LIST = False


def run(visited):
    spotify_client = Spotify.get_basic_client()

    # Cache artist data from get_related_artists() so we
    # don't need to call get_artist() later to get the same data
    artists_by_id = {}

    while True:
        try:
            artist_id = _get_seed_artists(limit=1)
            if not artist_id:
                break

            artist_id = artist_id[0]
            print("Starting search from artist {}".format(artist_id))

            start_time = time.time()
            last_index_time = time.time()

            stack = [artist_id]
            while stack:
                # Stop if it's not making much progress or it's past expiration
                curr_time = time.time()
                if curr_time - last_index_time > DELAYED_INDEX_TIME_LIMIT:
                    print("Delayed index time limit reached. Ending search...")
                    break
                if curr_time - start_time > WORKER_TIME_LIMIT:
                    print("Worker time limit reached. Ending search...")
                    break

                artist_id = stack.pop()
                if isinstance(artist_id, bytes):
                    artist_id = artist_id.decode("utf8")

                if artist_id not in visited:
                    visited.add(artist_id)

                    if Redis.artist_exists(artist_id=artist_id):
                        artist = Redis.get_artist(artist_id=artist_id)
                        related_artist_ids = set(artist["related_artists"] or [])
                    else:
                        if artist_id in artists_by_id:
                            artist = artists_by_id[artist_id]
                        else:
                            artist = spotify_client.get_artist(artist_id=artist_id)
                            if artist.get("id") != artist_id:
                                continue
                        top_tracks = spotify_client.get_top_tracks(artist_id=artist_id)
                        related_artists = spotify_client.get_related_artists(artist_id=artist_id)
                        for related_artist in related_artists:
                            related_artist_id = related_artist["id"]
                            artists_by_id[related_artist_id] = related_artist
                            # if not Redis.artist_exists(related_artist_id):
                            #     _top_tracks = spotify_client.get_top_tracks(artist_id=related_artist_id)
                            #     _related_artists = spotify_client.get_related_artists(artist_id=related_artist_id)
                            #     Redis.index_artist(
                            #         artist=related_artist, top_tracks=_top_tracks, related_artists=_related_artists)
                        Redis.index_artist(artist=artist, top_tracks=top_tracks, related_artists=related_artists)
                        related_artist_ids = set((related_artist["id"] for related_artist in related_artists))

                        last_index_time = time.time()

                        genre_ids = []
                        genres = artist.get("genres") or []
                        for genre in genres:
                            if Genre.genre_exists(name=genre):
                                genre_id = Genre.genre_name_to_id(name=genre)
                                genre_ids.append(genre_id)
                        if genre_ids:
                            Redis.index_artist_by_genres(artist_id=artist_id, genre_ids=genre_ids)

                    stack.extend(related_artist_ids - visited)
        except Exception as e:
            print(traceback.format_exc())


def _get_seed_artists(limit):
    # Find seed artists for our indexers
    artist_ids = set()
    if PICK_SEEDS_FROM_UNKNOWN_LIST:
        unknown_artist_ids = []
        with open("/tmp/unknown_related_artists.txt", "r") as f:
            unknown_artist_ids.extend(f.read().split(","))

        index = 0
        random.shuffle(unknown_artist_ids)
        while len(artist_ids) < limit and index < len(unknown_artist_ids):
            unknown_artist_id = unknown_artist_ids[index]
            if not Redis.artist_exists(artist_id=unknown_artist_id):
                artist_ids.add(unknown_artist_id)
            index += 1
    else:
        spotify_client = Spotify.get_basic_client()
        while len(artist_ids) < limit:
            choice = random.randint(0, 1)
            if choice == 0:
                # Select artist from random search
                letter_1 = random.choice(string.ascii_lowercase)
                letter_2 = random.choice(string.ascii_lowercase)
                query = "{}{}".format(letter_1, letter_2)
                random_wildcard_pos = random.randint(0, len(query))
                query = query[:random_wildcard_pos] + '*' + query[random_wildcard_pos:]
                search_results = spotify_client.search(q=query, type="artist", limit=50)
                for artist in search_results["artists"]["items"] or []:
                    if not Redis.artist_exists(artist_id=artist["id"]):
                        artist_ids.add(artist["id"])
            elif choice == 1:
                # Select artist from genre-seeded track recommendations
                genre_seed = random.choice(spotify_client.recommendation_genre_seeds()["genres"])
                genre_recommendations = spotify_client.recommendations(seed_genres=[genre_seed], limit=100)
                for track in genre_recommendations["tracks"] or []:
                    for artist in track["artists"] or []:
                        if not Redis.artist_exists(artist_id=artist["id"]):
                            artist_ids.add(artist["id"])

    return list(artist_ids)


if __name__ == "__main__":
    """
    Attempts to find all Spotify artists and index them in Redis.
    """

    Genre.load()

    visited = ConcurrentSet()
    if LOAD_VISITED_WITH_REDIS_ARTISTS:
        for artist_id in Redis.get_all_artist_ids():
            visited.add(artist_id)
        print("Loaded visited set with {} artists".format(len(visited)))

    try:
        # Start indexer workers
        with ThreadPoolExecutor(max_workers=NUM_WORKERS) as executor:
            futures = []
            for i in range(NUM_WORKERS):
                future = executor.submit(run, visited)
                futures.append(future)
            for future in as_completed(futures):
                print("Worker completed")
    except Exception as e:
        print(traceback.format_exc())
