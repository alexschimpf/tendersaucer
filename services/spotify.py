import spotipy
import os
from utils import log_time
from app_config import APP_CONFIG
from spotipy.oauth2 import SpotifyOAuth, SpotifyClientCredentials


class Spotify(spotipy.Spotify):

    SPOTIFY_CLIENT_ID = \
        APP_CONFIG.get("spotify", {}).get("client_id") or \
        os.getenv("SPOTIFY_CLIENT_ID")
    SPOTIFY_CLIENT_SECRET = \
        APP_CONFIG.get("spotify", {}).get("client_secret") or \
        os.getenv("SPOTIFY_CLIENT_SECRET")

    @classmethod
    def get_authentication_client(cls):
        return SpotifyOAuth(
            client_id=cls.SPOTIFY_CLIENT_ID,
            client_secret=cls.SPOTIFY_CLIENT_SECRET,
            redirect_uri=APP_CONFIG["spotify"]["redirect_uri"],
            scope="playlist-modify-public playlist-read-private user-top-read")

    @classmethod
    def is_access_token_valid(cls, access_token):
        try:
            spotify_client = cls.get_authorized_client(access_token=access_token)
            spotify_client.recommendation_genre_seeds()
        except spotipy.SpotifyException:
            return False

        return True

    @classmethod
    def get_authorized_client(cls, access_token):
        return Spotify(auth=access_token)

    @classmethod
    def get_basic_client(cls):
        credentials = SpotifyClientCredentials(
                client_id=cls.SPOTIFY_CLIENT_ID, client_secret=cls.SPOTIFY_CLIENT_SECRET)
        return Spotify(client_credentials_manager=credentials)

    @log_time(threshold=5)
    def get_related_artists(self, artist_id):
        return (self.artist_related_artists(artist_id=artist_id) or {}).get("artists") or []

    @log_time(threshold=5)
    def get_top_tracks(self, artist_id):
        return (self.artist_top_tracks(artist_id=artist_id) or {}).get("tracks") or []

    @log_time(threshold=5)
    def get_artist(self, artist_id):
        return self.artist(artist_id=artist_id)

    def get_user_top_artists(self, time_ranges=("medium_term",)):
        user_top_artist_ids = set()
        for time_range in time_ranges:
            top_artists = self.current_user_top_artists(time_range=time_range)
            while top_artists:
                top_artist_ids = [artist["id"] for artist in top_artists.get("items", [])]
                user_top_artist_ids.update(top_artist_ids)
                top_artists = self.next(top_artists)
        return user_top_artist_ids

    def get_user_familiar_artists(self):
        user_id = self.current_user()["id"]

        familiar_artists = set()
        playlists = self.current_user_playlists()
        while playlists:
            for playlist in playlists["items"]:
                if playlist["owner"]["id"] != user_id:
                    continue

                playlist_tracks = self.user_playlist_tracks(user=user_id, playlist_id=playlist["id"])
                while playlist_tracks:
                    for item in playlist_tracks["items"]:
                        artist_ids = [artist["id"] for artist in item["track"]["artists"]]
                        familiar_artists.update(artist_ids)
                    playlist_tracks = self.next(playlist_tracks)
            playlists = self.next(playlists)
        return familiar_artists

    def export_playlist(self, playlist_name, track_ids):
        spotify_user_id = self.me().get("id")
        new_playlist = self.user_playlist_create(spotify_user_id, playlist_name)
        self.user_playlist_add_tracks(spotify_user_id, new_playlist["id"], track_ids)
