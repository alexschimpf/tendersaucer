import redis
import pickle
import enum
from app_config import APP_CONFIG

SPOTIFY_CONN_POOL = redis.BlockingConnectionPool(**APP_CONFIG["redis"]["spotify"])
SESSION_CONN_POOL = redis.BlockingConnectionPool(**APP_CONFIG["redis"]["session"])


class ConnectionType(enum.Enum):
    SPOTIFY = 0
    SESSION = 1


def _get_redis_conn(conn_type):
    if conn_type == ConnectionType.SPOTIFY:
        return redis.StrictRedis(connection_pool=SPOTIFY_CONN_POOL)
    elif conn_type == ConnectionType.SESSION:
        return redis.StrictRedis(connection_pool=SESSION_CONN_POOL)


def get_genre_artists(genre_id):
    redis_conn = _get_redis_conn(conn_type=ConnectionType.SPOTIFY)
    genre = redis_conn.get(name="genre_{}".format(genre_id))
    if genre is None:
        return []
    return pickle.loads(genre) or []


def get_all_artist_ids():
    artist_ids = []
    redis_conn = _get_redis_conn(conn_type=ConnectionType.SPOTIFY)
    for key in redis_conn.scan_iter("artist_*"):
        artist_id = key[7:].decode("utf8")
        artist_ids.append(artist_id)
    return artist_ids


def get_artists(artist_ids):
    redis_conn = _get_redis_conn(conn_type=ConnectionType.SPOTIFY)
    keys = ("artist_{}".format(artist_id) for artist_id in artist_ids)
    artists = redis_conn.mget(keys=keys)

    artists_by_id = {}
    for artist in artists:
        if artist:
            artist = pickle.loads(artist)
            artists_by_id[artist["id"]] = artist
    return artists_by_id


def get_artist(artist_id):
    redis_conn = _get_redis_conn(conn_type=ConnectionType.SPOTIFY)
    artist = redis_conn.get(name="artist_{}".format(artist_id))
    return pickle.loads(artist)


def artist_exists(artist_id):
    redis_conn = _get_redis_conn(conn_type=ConnectionType.SPOTIFY)
    return redis_conn.exists(name="artist_{}".format(artist_id))


def genre_exists(genre_id):
    redis_conn = _get_redis_conn(conn_type=ConnectionType.SPOTIFY)
    return redis_conn.exists(name="genre_{}".format(genre_id))


def index_artist(artist, top_tracks, related_artists):
    if not artist:
        raise ValueError("Artist is empty!")

    indexed_artist = {
        "id": artist["id"],
        "name": artist["name"],
        "popularity": int(artist["popularity"] or 0),
        "top_tracks": [top_track["id"] for top_track in top_tracks],
        "related_artists": [related_artist["id"] for related_artist in related_artists]
    }

    redis_conn = _get_redis_conn(conn_type=ConnectionType.SPOTIFY)
    key = "artist_{}".format(artist["id"])
    redis_conn.set(name=key, value=pickle.dumps(indexed_artist))


def index_artist_by_genres(artist_id, genre_ids):
    if artist_id is None or not genre_ids:
        raise Exception("Artist id or genre ids are empty!")

    redis_conn = _get_redis_conn(conn_type=ConnectionType.SPOTIFY)
    for genre_id in genre_ids:
        key = "genre_{}".format(genre_id)
        artist_ids = redis_conn.get(name=key) or []
        if artist_ids:
            artist_ids = pickle.loads(artist_ids)
        artist_ids.append(artist_id)
        redis_conn.set(name=key, value=pickle.dumps(artist_ids))
