import time
import threading
import functools
from flask import request, Response


def log_time(threshold=0):
    def wrapper(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            start_time = time.time()
            result = func(*args, **kwargs)
            time_elapsed = time.time() - start_time
            if time_elapsed >= threshold:
                print("{}: time={}s".format(func.__name__, time_elapsed))
            return result
        return wrapped
    return wrapper


def params_required(params):
    def wrapper(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            missing_params = []
            for param in params:
                if request.args.get(param) is None:
                    missing_params.append(param)
            if missing_params:
                return Response("Missing params: {}".format(", ".join(missing_params)), 400)
            result = func(*args, **kwargs)
            return result
        return wrapped
    return wrapper


def catch_errors(func):
    @functools.wraps(func)
    def wrapped(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
        except Exception as e:
            return Response("There was an unexpected error.", 500)
        return result
    return wrapped


class ConcurrentSet(set):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lock = threading.RLock()

    def add(self, *args, **kwargs):
        self.lock.acquire()
        super().add(*args, **kwargs)
        self.lock.release()

    def __contains__(self, *args, **kwargs):
        self.lock.acquire()
        super().__contains__(*args, **kwargs)
        self.lock.release()
