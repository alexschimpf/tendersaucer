import random
import services.redis as Redis
from genres import Genre
from concurrent.futures import ThreadPoolExecutor, as_completed


MAX_SEED_ARTISTS = 10
MAX_GENRE_ARTISTS = 50
MAX_POPULARITY = 100
MAX_PLAYLIST_SIZE = 100
MAX_PLAYLIST_SIZE_PER_SEED = 50
NUM_WORKERS = 4
MAX_SEARCH_DEPTH = 5

NON_MUSICAL_ARTISTS = set()
for genre_name in Genre.NON_MUSICAL_GENRES:
    genre_id = Genre.genre_name_to_id(name=genre_name)
    NON_MUSICAL_ARTISTS.update(Redis.get_genre_artists(genre_id=genre_id))


def create_genres_playlist(seed_genre_ids, max_popularity, user_familiar_artists=()):
    # TODO: Add ability to create a playlist based on one or more genres
    pass


def create_genre_playlist(seed_genre_id, max_popularity, user_familiar_artists=()):
    if max_popularity < 0:
        raise ValueError("max_popularity must be >= 0")
    if max_popularity > MAX_POPULARITY:
        raise ValueError("max_popularity must be <= {}".format(MAX_POPULARITY))

    artist_ids = Redis.get_genre_artists(genre_id=seed_genre_id)
    if not artist_ids:
        return []

    artist_ids = set(filter(lambda x: x not in NON_MUSICAL_ARTISTS, artist_ids))

    # Filter artists that meet criteria
    playlist_artists = []
    sample_size = min(len(artist_ids), MAX_GENRE_ARTISTS)
    artist_ids = random.sample(artist_ids, sample_size)
    artists_by_id = Redis.get_artists(artist_ids=artist_ids)
    for artist_id, artist in artists_by_id.items():
        popularity = artist["popularity"] or 0
        if popularity <= max_popularity and artist_id not in user_familiar_artists:
            playlist_artists.append(artist)

    # Compile randomly selected tracks from artists
    playlist = []
    for artist in playlist_artists:
        top_tracks = list(set(artist["top_tracks"] or ()))
        if top_tracks:
            playlist.append(random.choice(top_tracks))

    return random.sample(playlist, min(len(playlist), MAX_PLAYLIST_SIZE))


def create_artist_playlist(seed_artist_ids, max_popularity, max_depth, user_familiar_artists=()):
    if max_popularity < 0:
        raise ValueError("max_popularity must be >= 0")
    if max_popularity > MAX_POPULARITY:
        raise ValueError("max_popularity must be <= {}".format(MAX_POPULARITY))
    if max_depth <= 0:
        raise ValueError("max_depth must be > 0")
    if max_depth > MAX_SEARCH_DEPTH:
        raise ValueError("max_depth must be <= {}".format(MAX_SEARCH_DEPTH))
    if not seed_artist_ids:
        raise ValueError("You must supply at least one seed artist!")

    seed_artist_ids = set(filter(lambda x: x not in NON_MUSICAL_ARTISTS, seed_artist_ids))
    if len(seed_artist_ids) > MAX_SEED_ARTISTS:
        seed_artist_ids = random.sample(seed_artist_ids, MAX_SEED_ARTISTS)

    playlist = []
    playlist_artists = set()
    with ThreadPoolExecutor(max_workers=NUM_WORKERS) as executor:
        futures = []
        for artist_id in seed_artist_ids:
            future = executor.submit(
                _search_related_artists, artist_id, max_popularity,
                playlist_artists, max_depth, user_familiar_artists)
            futures.append(future)
        for future in as_completed(futures):
            playlist.extend(future.result() or [])

    return random.sample(playlist, min(len(playlist), MAX_PLAYLIST_SIZE))


def _search_related_artists(artist_id, max_popularity, playlist_artists, max_depth, user_familiar_artists=()):
    visited = set()
    stack = [(0, artist_id)]
    artists_by_id = {}
    playlist = []
    while stack:
        depth, artist_id = stack.pop()

        if artist_id not in visited:
            visited.add(artist_id)

            if artist_id in NON_MUSICAL_ARTISTS or artist_id in playlist_artists:
                continue

            if artist_id in artists_by_id:
                artist = artists_by_id[artist_id]
            else:
                artist = Redis.get_artist(artist_id=artist_id)
                artists_by_id[artist_id] = artist

            popularity = artist["popularity"] or 0
            if popularity <= max_popularity:
                top_tracks = list(set(artist["top_tracks"] or ()))
                if top_tracks:
                    track_id = random.choice(top_tracks)
                    if track_id and track_id not in playlist and artist_id not in playlist_artists and \
                            artist_id not in user_familiar_artists:
                        playlist_artists.add(artist_id)
                        playlist.append(track_id)

            if depth < max_depth:
                related_artist_ids = set(artist["related_artists"] or []) - visited
                if related_artist_ids:
                    related_artists = Redis.get_artists(artist_ids=related_artist_ids)
                    for related_artist_id, related_artist in related_artists.items():
                        artists_by_id[related_artist_id] = related_artist

                    stack.extend(map(lambda x: (depth + 1, x), related_artist_ids))

    return random.sample(playlist, min(len(playlist), MAX_PLAYLIST_SIZE_PER_SEED))
