import os
import redis
import playlist
import datetime
import services.redis as Redis
from services.spotify import Spotify
from spotipy import SpotifyException
from genres import Genre
from app_config import APP_CONFIG
from flask_session import Session
from utils import params_required, catch_errors
from flask import Flask, Response, request, render_template, session, redirect

Genre.load()

app = Flask(__name__, template_folder="static/templates")
app.secret_key = os.urandom(24)
app.config.update({
    "SESSION_TYPE": "redis",
    "SESSION_KEY_PREFIX": "session:",
    "PERMANENT_SESSION_LIFETIME": datetime.timedelta(hours=8),
    "SESSION_REDIS": redis.Redis(connection_pool=Redis.SESSION_CONN_POOL),
    "SECRET_KEY": APP_CONFIG.get("session_secret_key") or os.getenv("SESSION_SECRET_KEY")
})
Session(app)


@app.route("/", methods=["GET"])
def index():
    return render_template("index.html", genres=sorted(Genre.get_genres(exclude_non_musical=True)))


@app.route("/logout", methods=["GET"])
def logout():
    session.clear()
    return redirect("/")


@app.route("/spotify_authentication", methods=["GET"])
def spotify_authentication():
    access_token = session.get("spotify_access_token")
    if not Spotify.is_access_token_valid(access_token=access_token):
        auth_client = Spotify.get_authentication_client()
        return redirect(auth_client.get_authorize_url())

    return redirect("/")


@app.route("/spotify_callback", methods=["GET"])
def spotify_callback():
    auth_client = Spotify.get_authentication_client()
    spotify_access_code = auth_client.parse_response_code(request.url)
    spotify_access_token = auth_client.get_access_token(spotify_access_code)
    if spotify_access_token:
        session["spotify_access_token"] = spotify_access_token.get("access_token")
    return redirect("/")


@app.route("/web_create_genre_playlist", methods=["GET"])
@params_required(params=("genre", "max_popularity"))
@catch_errors
def web_create_genre_playlist():
    seed_genre_id = Genre.genre_name_to_id(request.args.get("genre"))
    max_popularity = int(request.args.get("max_popularity"))
    exclude_playlist_artists = request.args.get("exclude_playlist_artists") == "on"

    spotify_client = Spotify.get_authorized_client(
        access_token=session["spotify_access_token"])

    if exclude_playlist_artists:
        user_familiar_artists = spotify_client.get_user_familiar_artists()
    else:
        user_familiar_artists = ()

    track_ids = playlist.create_genre_playlist(
        seed_genre_id=seed_genre_id, max_popularity=max_popularity,
        user_familiar_artists=user_familiar_artists)
    if not track_ids:
        raise Exception("No tracks met the criteria!")
    spotify_client.export_playlist(playlist_name="tendersaucer mix", track_ids=track_ids)

    return Response("Ok", 200)


@app.route("/web_create_artist_playlist", methods=["GET"])
@params_required(params=("max_popularity", "max_depth"))
@catch_errors
def web_create_artist_playlist():
    max_popularity = int(request.args.get("max_popularity"))
    max_depth = int(request.args.get("max_depth"))
    exclude_playlist_artists = request.args.get("exclude_playlist_artists") == "on"

    spotify_client = Spotify.get_authorized_client(
        access_token=session["spotify_access_token"])

    if exclude_playlist_artists:
        user_familiar_artists = spotify_client.get_user_familiar_artists()
    else:
        user_familiar_artists = ()

    seed_artist_ids = []
    user_top_artists = spotify_client.get_user_top_artists()
    for artist_id in user_top_artists:
        if Redis.artist_exists(artist_id=artist_id):
            seed_artist_ids.append(artist_id)

    track_ids = playlist.create_artist_playlist(
        seed_artist_ids=seed_artist_ids, max_popularity=max_popularity,
        user_familiar_artists=user_familiar_artists, max_depth=max_depth)
    if not track_ids:
        raise Exception("No tracks met the criteria!")
    spotify_client.export_playlist(playlist_name="tendersaucer mix", track_ids=track_ids)

    return Response("Ok", 200)


@app.route("/mobile_create_genre_playlist", methods=["GET"])
@params_required(params=("genre", "max_popularity"))
@catch_errors
def mobile_create_genre_playlist():
    pass

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
