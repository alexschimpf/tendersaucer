class Genre(object):

    NON_MUSICAL_GENRES = ('deep comedy', 'comedy', 'comedy_rock')

    _GENRES = None
    _GENRE_ID_TO_NAME = {}
    _GENRE_NAME_TO_ID = {}

    @classmethod
    def load(cls):
        with open("static/genres.txt", "r") as f:
            lines = f.read().split("\n")
            for index, line in enumerate(lines):
                if not line:
                    continue
                genre = line.strip().lower()
                cls._GENRE_ID_TO_NAME[index + 1] = genre
                cls._GENRE_NAME_TO_ID[genre] = index + 1
        cls._GENRES = frozenset(cls._GENRE_ID_TO_NAME.values())

    @classmethod
    def genre_exists(cls, name):
        return name in cls._GENRES

    @classmethod
    def get_genres(cls, exclude_non_musical=False):
        genres = cls._GENRES
        if exclude_non_musical:
            genres = frozenset(
                filter(lambda x: x not in cls.NON_MUSICAL_GENRES, genres))
        return genres

    @classmethod
    def get_genres_by_id(cls):
        return cls._GENRE_ID_TO_NAME

    @classmethod
    def genre_id_to_name(cls, id):
        return cls._GENRE_ID_TO_NAME.get(id)

    @classmethod
    def genre_name_to_id(cls, name):
        return cls._GENRE_NAME_TO_ID.get((name or '').lower())
